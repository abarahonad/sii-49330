// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include <sys/types.h>	//include Practica2
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "Plano.h"
#include "Raqueta.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Esfera.h"
//#include "DatosMemCompartida.h"

#include <pthread.h>

#include "Socket.h"		//Lo añado para la practica 4

class MundoServidor  
{
public:
	void Init();
	MundoServidor();
	virtual ~MundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int fd2;		//Practica2
	//int fd3;		//Practica3
	//int fd3b;		//Practica3

	pthread_t thid1;	//Practica3

	//Socket s_conexion;	//Practica 4

	//Practica5
	Socket servidor;
	std::vector<Socket> conexiones;
	//void GestionaConexiones();
	pthread_t thid2, thid3;
	int acabar;
	void RecibeComandosJugador2();
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
