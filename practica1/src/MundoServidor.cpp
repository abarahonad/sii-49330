// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <iostream>		//Modificaciones Practica2
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>

#define TAM_BUFFER 60

void* hilo_comandos(void*);
void* hilo_comandos2(void*);
void* GestionaConexiones(void*);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MundoServidor::MundoServidor()
{
	Init();
}

MundoServidor::~MundoServidor()
{
	if(close(fd2)==-1){	//Practica2
		perror("Fallo a cerrar la tuberia p2");
		exit(1);
	}
	/*if(close(fd3)==-1){	//Practica3
		perror("Fallo a cerrar la tuberia p3");
		exit(1);
	}
	if(close(fd3b)==-1){	//Practica3
		perror("Fallo a cerrar la tuberia p3b");
		exit(1);
	}*/
	
	//pthread_join(thid1,0);
	//pthread_exit(0);

	//s_conexion.Close();
	//s_comunicacion.Close();

	//Practica5
	pthread_join(thid1,0);
	pthread_join(thid2,0);
	pthread_join(thid3,0);
	acabar=1;
	servidor.Close();
	for(int i=0;i<conexiones.size();i++){
		conexiones[i].Close();
	}
}

void MundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void MundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	char buffer[TAM_BUFFER];	//Practica2

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		sprintf(buffer,"Jugador 2 marca 1 punto, lleva un total de %d\n",puntos2);	//Practica2
		if(write(fd2,buffer,sizeof(buffer))==-1){	//Practica2
			perror("Fallo en la escritura de la tuberia p2");
			exit(1);
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		sprintf(buffer,"Jugador 1 marca 1 punto, lleva un total de %d\n",puntos1);	//Practica2
		if(write(fd2,buffer,sizeof(buffer))==-1){	//Practica2
			perror("Fallo en la escritura de la tuberia p2");
			exit(1);
		}
	}

	if(puntos1>=3||puntos2>=3)	//Practica2
		exit(0);

	char cad[200];		//Practica3
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);

	/*if(write(fd3,cad,sizeof(cad))==-1){
		perror("Fallo al escribir en la tuberia p3");
		exit(1);
	}*/

	/*if(s_comunicacion.Send(cad,sizeof(cad))==-1){
		perror("Fallo en el envio por socket");
		exit(1);
	}*/

	for(i=conexiones.size()-1; i>=0; i--){
		if(conexiones[i].Send(cad,sizeof(cad)) <= 0){
			conexiones.erase(conexiones.begin()+i);
			if(i<2)	{puntos1=puntos2=0;}
		}
	}
}

void MundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
}

void MundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	fd2=open("/tmp/pipe_name",O_WRONLY);	//Practica2
	if(fd2==-1){
		perror("Fallo al abrir la tuberia p2");
		exit(1);
	}

	/*fd3=open("/tmp/pipe_p3",O_WRONLY);	//Practica3
	if(fd3==-1){
		perror("Fallo al abrir la tuberia p3");
		exit(1);
	}

	fd3b=open("/tmp/pipe_p3b",O_RDONLY);	//Practica3	
	if(fd3b==-1){
		perror("Fallo al abrir la tuberia p3b");
		exit(1);
	}*/

	//Creacion del hilo
	pthread_create(&thid1, NULL, hilo_comandos, this);

	//Practica 4
	char ip[]="127.0.0.1";
	int puerto=3550;
	char cad[200];
	if(servidor.InitServer(ip,puerto)==-1){
		perror("Fallo al iniciar el socket");
		exit(1);
	}
	/*s_comunicacion=s_conexion.Accept();
	int error=s_comunicacion.Receive(cad,sizeof(cad));
	/*if(error==-1){
		perror("Fallo al recibir dato por socket");
		exit(1);
	}*/
	//printf("%s\n",cad);

	//Practica5
	acabar=0;
	pthread_create(&thid2, NULL, GestionaConexiones, this);
	pthread_create(&thid3, NULL, hilo_comandos2, this);
}

void* hilo_comandos(void* d){
	MundoServidor* p=(MundoServidor*) d;
	p->RecibeComandosJugador();
}

void MundoServidor::RecibeComandosJugador(){
	while(!acabar){
		usleep(10);
		char cad[200];
		/*if(read(fd3b,&cad,sizeof(cad))==-1){
			perror("Fallo al leer tuberia p3b");
			exit(1);
		}*/
		
		int error=conexiones[0].Receive(cad,sizeof(cad));
		/*if(error==-1){
			perror("Fallo al recibir dato por socket");
			exit(1);
		}*/
		unsigned char key;
		sscanf(cad, "%c", &key);
		switch(key)
		{
		//	case 'a':jugador1.velocidad.x=-1;break;
		//	case 'd':jugador1.velocidad.x=1;break;
			case 's':jugador1.velocidad.y=-4;break;
			case 'w':jugador1.velocidad.y=4;break;
			//case 'l':jugador2.velocidad.y=-4;break;
			//case 'o':jugador2.velocidad.y=4;break;

		}
	}
}

void* GestionaConexiones(void* d){
	MundoServidor* p=(MundoServidor*) d;
	while(!(p->acabar)){
		Socket s=p->servidor.Accept();
		if(s.getSock()!=-1){
			p->conexiones.push_back(s);
		}
	}
}

void* hilo_comandos2(void* d){
	MundoServidor* p=(MundoServidor*) d;
	p->RecibeComandosJugador2();
}

void MundoServidor::RecibeComandosJugador2(){
	while(!acabar){
		usleep(10);
		char cad[200];
		/*if(read(fd3b,&cad,sizeof(cad))==-1){
			perror("Fallo al leer tuberia p3b");
			exit(1);
		}*/
		
		int error=conexiones[1].Receive(cad,sizeof(cad));
		/*if(error==-1){
			perror("Fallo al recibir dato por socket");
			exit(1);
		}*/
		unsigned char key;
		sscanf(cad, "%c", &key);
		switch(key)
		{
		//	case 'a':jugador1.velocidad.x=-1;break;
		//	case 'd':jugador1.velocidad.x=1;break;
			//case 's':jugador1.velocidad.y=-4;break;
			//case 'w':jugador1.velocidad.y=4;break;
			case 'l':jugador2.velocidad.y=-4;break;
			case 'o':jugador2.velocidad.y=4;break;

		}
	}
}
